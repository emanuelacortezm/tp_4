<img src="/fotos/unsl.png" width="100">



# **Trabajo Práctico 4**
## Arquitectura de Computadoras
### Camino de datos y control del microprocesador


#### **Profesores:**
###### Ing. Andres Airabella
###### Ing. Astrid Andrada

1. **Genere dibujos independientes para cada uno de los bloques que va a utilizar para construir el procesador**

>Se generan los siguientes bloques independientes para cada uno de los bloques que se necesitan para construir el procesador:

![Ejercicio 1](/fotos/ej_1.png)


2. **Realice el dibujo de un Datapath completo para el set de instrucciones propuesto. Indique en el dibujo anchos de todos los buses y nombres de las señales intermedias que luego utilizará en el código.**

>Se procede a realizar el Datapath del procesador tilizando la librería *circuitikz*. Se obtiene el siguiente *Datapath*:

![Ejercicio 2](/fotos/ej_2.png)

3. **Realice una tabla de verdad para todas las señales de control.** 

>Se realiza una tabla de verdad con todas las señales de control:

| Entrada o salida | Nombre de la señal | R-format | ld  | sd  | beq |
|:----------------:|:------------------:|:--------:|:---:|:---:|:---:|
|     Entrada      |       I\[6\]       |    0     |  0  |  0  |  1  |
|                  |       I\[5\]       |    1     |  0  |  1  |  1  |
|                  |       I\[4\]       |    1     |  0  |  0  |  0  |
|                  |       I\[3\]       |    0     |  0  |  0  |  0  |
|                  |       I\[2\]       |    0     |  0  |  0  |  0  |
|                  |       I\[1\]       |    1     |  1  |  1  |  1  |
|                  |       I\[0\]       |    1     |  1  |  1  |  1  |
|      Salida      |       ALUSrc       |    0     |  1  |  1  |  0  |
|                  |      MemtoReg      |    0     |  1  |  X  |  X  |
|                  |      RegWrite      |    1     |  1  |  0  |  0  |
|                  |      MemRead       |    0     |  1  |  0  |  0  |
|                  |      MemWrite      |    0     |  0  |  1  |  0  |
|                  |       Branch       |    0     |  0  |  0  |  1  |
|                  |       ALUOp1       |    1     |  0  |  0  |  0  |
|                  |       ALUOp0       |    0     |  0  |  0  |  1  |

***Tabla de verdad con las entradas y salidas de las señales de control.***

4. **Cree un repositorio en www.gitlab.com siguiendo este tutorial:https://alejandrojs.wordpress.com/2017/06/01/como-empezar-a-usar-git-con-gitlab/**

>Se crea el repositorio, el cual se puede acceder a través del siguiente link:
[Repositorio TP4](https://gitlab.com/emanuelacortezm/tp_4)

5. **Elabore un informe con el desarrollo de los ejercicios. Este informe deberá escribirse dentro del mismo repositorio creado en el ejercicio anterior, utilizando el formato “Markdown”.**

>Se elaboró este informe en ***Markdown***.
