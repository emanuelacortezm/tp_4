library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity alu is

generic (N : integer :=64);
port (
      A_i      : in  std_logic_vector(N - 1 downto 0);
      B_i      : in  std_logic_vector(N - 1 downto 0);
      OUTPUT_o : out std_logic_vector(N - 1 downto 0);
      ALUop_i  : in  std_logic_vector(3 downto 0);
      Zero_o   : out std_logic
      );
end entity;

architecture alu_arch of alu is

constant AND_IN : std_logic_vector(3 downto 0) := "0000";  -- AND Lógico
constant OR_IN  : std_logic_vector(3 downto 0) := "0001";  -- OR Lógico
constant ADD    : std_logic_vector(3 downto 0) := "0010";  -- SUMA
constant XOR_IN : std_logic_vector(3 downto 0) := "0011";  -- XOR Lógico
constant SUB    : std_logic_vector(3 downto 0) := "0100";  -- RESTA
constant SLL_IN : std_logic_vector(3 downto 0) := "0101";  --  Shifteo izquierdo lógico
constant SRL_IN : std_logic_vector(3 downto 0) := "0110";  --  Shifteo derecho lógico
constant SRA_IN : std_logic_vector(3 downto 0) := "1000";  --  Shifteo derecho aritmético

constant EQ     : std_logic_vector(3 downto 0) := "1001"; -- Salto si son iguales
constant NEQ    : std_logic_vector(3 downto 0) := "1010"; -- Salto si son distintos
constant BLT   : std_logic_vector(3 downto 0) := "1011"; -- Si A menor que B
constant BGE : std_logic_vector(3 downto 0) := "1100"; -- Si A es mayor o igual que B


signal output_s : std_logic_vector(N-1 downto 0);
signal zero : std_logic;

begin

process(A_i, B_i, ALUop_i)

variable aux1 : std_logic;
variable aux2 : std_logic_vector(N-1 downto 0);

begin
    case ALUop_i is
        when ADD =>
            output_s <= std_logic_vector((unsigned(A_i) + unsigned(B_i)));
            
        when SUB =>
            output_s <= std_logic_vector((unsigned(A_i) - unsigned(B_i)));
            
        when AND_IN =>
            output_s <= A_i and B_i;
            
        when OR_IN =>
            output_s <= A_i or B_i;
            
        when XOR_IN =>
            output_s <= A_i xor B_i;
            
        when SLL_IN =>
            output_s <= std_logic_vector(shift_left(unsigned(A_i), to_integer(unsigned(B_i))));
            
        when SRL_IN =>
            output_s <= std_logic_vector(shift_right(unsigned(A_i), to_integer(unsigned(B_i))));
            
        
        when SRA_IN =>
            output_s <= std_logic_vector(shift_right(signed(A_i), to_integer(unsigned(B_i))));
            
		when EQ =>  
            output_s <= (others => '0');
                if (A_i = B_i) then
	                zero <= '1';		        
	             else  
                    zero <= '0';
	            end if;
		when NEQ =>  
            output_s <= (others => '0');
                if (A_i /= B_i) then
	                zero <= '0';		        
	             else  
                    zero <= '1';
                end if;  
    	when BLT => 
            output_s <= (others => '0');
                if (A_i < B_i) then
	                zero <= '1';		        
	             else  
                    zero <= '0';
	             end if; 

    	when BGE => output_s <= (others => '0');
                if (A_i >= B_i) then
	                zero <= '1';		        
	            else  
                    zero <= '0';
	            end if;
    	when others => output_s <= (others => '0');
		    zero <= '0';		      
    end case;
        
end process;

Zero_o <= zero;
OUTPUT_o <= output_s;
    
end alu_arch;
